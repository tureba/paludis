#!/usr/bin/env bash
# vim: set ft=sh sw=4 sts=4 et :

mkdir continue_on_failure_TEST_dir || exit 1
cd continue_on_failure_TEST_dir || exit 1
mkdir -p build

mkdir -p config/.paludis-continue-on-failure-test/repositories
cp ../../../../paludis/environments/paludis/tests_output.conf config/.paludis-continue-on-failure-test/output.conf
cat <<END > config/.paludis-continue-on-failure-test/specpath.conf
config-suffix =
END

cat <<END > config/.paludis-continue-on-failure-test/options.conf
*/* foo
END

cat <<END > config/.paludis-continue-on-failure-test/licences.conf
*/* *
END

cat <<END > config/.paludis-continue-on-failure-test/platforms.conf
*/* test
END

cat <<END > config/.paludis-continue-on-failure-test/general.conf
world = `pwd`/root/world
END

cat <<END > config/.paludis-continue-on-failure-test/bashrc
export CHOST="my-chost"
END

cat <<END > config/.paludis-continue-on-failure-test/repositories/repo1.conf
location = `pwd`/repo1
format = e
profiles = \${location}/profiles/testprofile
builddir = `pwd`/build
END

cat <<END > config/.paludis-continue-on-failure-test/repositories/installed.conf
location = `pwd`/root/var/db/pkg
format = vdb
builddir = `pwd`/build
END

mkdir -p root/tmp
mkdir -p root/var/db/pkg
mkdir -p root/${SYSCONFDIR}
touch root/${SYSCONFDIR}/ld.so.conf

mkdir -p repo1/{distfiles,profiles/testprofile,packages/cat/{a,b,c,d,e,u,v,w,x,y,z}/files} || exit 1

cd repo1 || exit 1
echo "test-repo-1" > profiles/repo_name || exit 1
cat <<END > profiles/testprofile/make.defaults
ARCH=test
USERLAND=test
KERNEL=test
TESTPROFILE_WAS_SOURCED=yes
PROFILE_ORDERING=1
USE_EXPAND="USERLAND KERNEL"
END

cat <<"END" > packages/cat/a/a-1.exheres-0 || exit 1
DESCRIPTION="Test a"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/d"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/a
}
END

cat <<"END" > packages/cat/b/b-1.exheres-0 || exit 1
DESCRIPTION="Test b"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/a"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/b
}
END

cat <<"END" > packages/cat/c/c-1.exheres-0 || exit 1
DESCRIPTION="Test c"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/e"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/c
}
END

cat <<"END" > packages/cat/d/d-1.exheres-0 || exit 1
DESCRIPTION="Test d"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES=""
WORK=$WORKBASE

pkg_setup() {
    die "supposed to fail"
}
END

cat <<"END" > packages/cat/e/e-1.exheres-0 || exit 1
DESCRIPTION="Test e"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES=""
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/e
}
END

cat <<"END" > packages/cat/z/z-1.exheres-0 || exit 1
DESCRIPTION="Test z"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/w"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/z
}
END

cat <<"END" > packages/cat/y/y-1.exheres-0 || exit 1
DESCRIPTION="Test y"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/u"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/y
}
END

cat <<"END" > packages/cat/x/x-1.exheres-0 || exit 1
DESCRIPTION="Test x"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/v"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/x
}
END

cat <<"END" > packages/cat/w/w-1.exheres-0 || exit 1
DESCRIPTION="Test w"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES=""
WORK=$WORKBASE
END

cat <<"END" > packages/cat/w/w-2.exheres-0 || exit 1
DESCRIPTION="Test w"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES=""
WORK=$WORKBASE

pkg_setup() {
    die "supposed to fail"
}
END

cat <<"END" > packages/cat/v/v-1.exheres-0 || exit 1
DESCRIPTION="Test v"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES=""
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/v
}
END

cat <<"END" > packages/cat/u/u-1.exheres-0 || exit 1
DESCRIPTION="Test u"
HOMEPAGE="http://paludis.exherbo.org/"
DOWNLOADS=""
SLOT="0"
MYOPTIONS=""
LICENCES="GPL-2"
PLATFORMS="test"
DEPENDENCIES="cat/w"
WORK=$WORKBASE

src_install() {
    mkdir -p ${TEST_ROOT}
    touch ${TEST_ROOT}/u
}
END

cd ..

