" Vim syntax file
" Language:     Paludis licences.conf files
" Author:       Ciaran McCreesh
" Copyright:    Copyright (c) 2007, 2011 Ciaran McCreesh
" Licence:      You may redistribute this under the same terms as Vim itself
"
" Syntax highlighting for Paludis licences.conf files.
"

if &compatible || v:version < 700
    finish
endif

if exists("b:current_syntax")
  finish
endif

syn region PaludisLicencesConfComment start=/^\s*#/ end=/$/

syn match  PaludisLicencesConfPDS /^[^ \t#\/]\+\/[^ \t#\/]\+\s*/
            \ nextgroup=PaludisLicencesConfLicence,PaludisLicencesConfLicenceGroup,PaludisLicencesConfContinuation
            \ contains=PaludisLicencesConfWildcard
syn match  PaludisLicencesConfWildcard contained /\(\*\/\@=\|\/\@<=\*\)/
syn match  PaludisLicencesConfSet /^[^ \t#\/]\+\S\@!/
            \ nextgroup=PaludisLicencesConfLicence,PaludisLicencesConfLicenceGroup,PaludisLicencesConfContinuation skipwhite
syn match  PaludisLicencesConfLicence contained /-\?[a-zA-Z0-9\-_*.]\+/
            \ nextgroup=PaludisLicencesConfLicence,PaludisLicencesConfLicenceGroup,PaludisLicencesConfContinuation skipwhite
syn match  PaludisLicencesConfLicenceGroup contained /-\?@[a-zA-Z0-9\-_*]\+/
            \ nextgroup=PaludisLicencesConfLicence,PaludisLicencesConfLicenceGroup,PaludisLicencesConfContinuation skipwhite
syn match  PaludisLicencesConfContinuation contained /\\$/
            \ nextgroup=PaludisLicencesConfLicence,PaludisLicencesConfLicenceGroup,PaludisLicencesConfContinuation skipwhite skipnl

hi def link PaludisLicencesConfComment          Comment
hi def link PaludisLicencesConfPDS              Identifier
hi def link PaludisLicencesConfWildcard         Special
hi def link PaludisLicencesConfSet              Special
hi def link PaludisLicencesConfLicence          Keyword
hi def link PaludisLicencesConfLicenceGroup     Macro
hi def link PaludisLicencesConfContinuation     Preproc

let b:current_syntax = "paludis-licences-conf"


