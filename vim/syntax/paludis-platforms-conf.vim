" Vim syntax file
" Language:     Paludis platforms.conf files
" Author:       Ciaran McCreesh
" Copyright:    Copyright (c) 2007 Ciaran McCreesh
" Licence:      You may redistribute this under the same terms as Vim itself
"
" Syntax highlighting for Paludis platforms.conf files.
"

if &compatible || v:version < 700
    finish
endif

if exists("b:current_syntax")
  finish
endif

syn region PaludisPlatformsConfComment start=/^\s*#/ end=/$/

syn match  PaludisPlatformsConfPDS /^[^ \t#\/]\+\/[^ \t#\/]\+\s*/
            \ nextgroup=PaludisPlatformsConfPlatform,PaludisPlatformsConfContinuation
            \ contains=PaludisPlatformsConfWildcard
syn match  PaludisPlatformsConfWildcard contained /\(\*\/\@=\|\/\@<=\*\)/
syn match  PaludisPlatformsConfSet /^[^ \t#\/]\+\S\@!/
            \ nextgroup=PaludisPlatformsConfPlatform,PaludisPlatformsConfContinuation skipwhite
syn match  PaludisPlatformsConfPlatform contained /-\?\~\?[a-zA-Z0-9\-_*]\+/
            \ nextgroup=PaludisPlatformsConfPlatform,PaludisPlatformsConfContinuation skipwhite
syn match  PaludisPlatformsConfContinuation contained /\\$/
            \ nextgroup=PaludisPlatformsConfPlatform,PaludisPlatformsConfContinuation skipwhite skipnl

hi def link PaludisPlatformsConfComment          Comment
hi def link PaludisPlatformsConfPDS              Identifier
hi def link PaludisPlatformsConfWildcard         Special
hi def link PaludisPlatformsConfSet              Special
hi def link PaludisPlatformsConfPlatform          Platform
hi def link PaludisPlatformsConfContinuation     Preproc

let b:current_syntax = "paludis-platforms-conf"

