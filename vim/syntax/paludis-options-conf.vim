" Vim syntax file
" Language:     Paludis options.conf files
" Author:       Ciaran McCreesh
" Copyright:    Copyright (c) 2007, 2009, 2010 Ciaran McCreesh
" Licence:      You may redistribute this under the same terms as Vim itself
"
" Syntax highlighting for Paludis options.conf files.
"

if &compatible || v:version < 700
    finish
endif

if exists("b:current_syntax")
  finish
endif

syn region PaludisOptionsConfComment start=/^\s*#/ end=/$/

syn match  PaludisOptionsConfPDS /^[^ \t#\/]\+\/[^ \t#\/]\+\s*/
            \ nextgroup=PaludisOptionsConfFlag,PaludisOptionsConfPrefix,PaludisOptionsConfContinuation
            \ contains=PaludisOptionsConfWildcard
syn match  PaludisOptionsConfWildcard contained /\(\*\/\@=\|\/\@<=\*\)/
syn match  PaludisOptionsConfSet /^[^ \t#\/]\+\S\@!/
            \ nextgroup=PaludisOptionsConfFlag,PaludisOptionsConfPrefix,PaludisOptionsConfContinuation skipwhite
syn match  PaludisOptionsConfFlag contained /\S\@<!(\?[a-zA-Z0-9\-\._*+]\+)\?\(\S\@!\|=\@=\)/
            \ nextgroup=PaludisOptionsConfFlag,PaludisOptionsConfFlagValue,PaludisOptionsConfPrefix,PaludisOptionsConfContinuation skipwhite
syn match  PaludisOptionsConfFlagValue contained /=\S*/
            \ nextgroup=PaludisOptionsConfFlag,PaludisOptionsConfPrefix,PaludisOptionsConfContinuation skipwhite
syn match  PaludisOptionsConfPrefix contained /[a-zA-Z0-9_*][a-zA-Z0-9\-_*]*:/
            \ nextgroup=PaludisOptionsConfFlag,PaludisOptionsConfPrefix,PaludisOptionsConfContinuation skipwhite
syn match  PaludisOptionsConfContinuation contained /\\$/
            \ nextgroup=PaludisOptionsConfFlag,PaludisOptionsConfPrefix,PaludisOptionsConfContinuation skipwhite skipnl

hi def link PaludisOptionsConfComment          Comment
hi def link PaludisOptionsConfPDS              Identifier
hi def link PaludisOptionsConfSet              Special
hi def link PaludisOptionsConfWildcard         Special
hi def link PaludisOptionsConfPrefix           Constant
hi def link PaludisOptionsConfFlag             Keyword
hi def link PaludisOptionsConfFlagValue        Type
hi def link PaludisOptionsConfContinuation     Preproc

let b:current_syntax = "paludis-options-conf"


