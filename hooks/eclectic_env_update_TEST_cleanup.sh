#!/usr/bin/env bash
# vim: set ft=sh sw=4 sts=4 et :

if [ -d eclectic_env_update_TEST_dir ] ; then
    rm -fr eclectic_env_update_TEST_dir
else
    true
fi

