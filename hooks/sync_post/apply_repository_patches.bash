
dir=/var/db/paludis/repositories/${TARGET}
pdir=/etc/paludis/patches/${TARGET}

[[ -e "${dir}" && -e "${pdir}" ]] || exit 0

find -L "${pdir}" -type f -name '*.patch' -print0 | xargs -0 -r git -C "${dir}" am || ( git -C "${dir}" am --abort; exit 1 )

