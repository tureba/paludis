cd "${S}"
for patchdir in /etc/paludis/patches/${CATEGORY}/${PN}{,/$SLOT}; do
	if [[ -d $patchdir ]] ; then  
		for p in $patchdir/*.patch ; do
			[[ -f "${p}" ]] || continue
			einfo "patching with $p"
			patch -p1 < ${p} || exit 1
		done
	fi
done
