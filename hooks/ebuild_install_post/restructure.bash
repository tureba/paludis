# vim: set sw=4 sts=4 et :

[[ -d "${IMAGE}" ]] || exit 0

edo pushd "${IMAGE}"

host=$(exhost --target)

# skeleton cleanup
[[ -L etc ]] && rm etc
[[ -L var ]] && rm var
[[ -L usr/share/doc/${PNV} ]] && rm usr/share/doc/${PNV}
[[ -L usr/share/doc/${PNVR} ]] && rm usr/share/doc/${PNVR}

local dirs=( ) tmpfiles_conf="${IMAGE}/usr/$host/lib/tmpfiles.d/${CATEGORY}--${PN}:${SLOT}.conf"

mkdir -p "${IMAGE}/usr/share/factory/" "${IMAGE}/usr/$host/lib/tmpfiles.d/"
edo pushd "${IMAGE}/usr/share/factory/"

# remove empty directories
find etc var -type d -empty -delete 2>/dev/null

dirs=(
    # top level files are copied
    $(find etc var -mindepth 1 -maxdepth 1 ! -type d -printf '/%p\n' 2>/dev/null)
    # for files in other directories, copy the parent directories
    $(find etc var -mindepth 2 ! -type d -printf '/%h\n' 2>/dev/null | sort -u)
)
if [[ ${#dirs[@]} -gt 0 ]]; then
    for p in ${dirs[@]}; do
        if ! grep -q -e "\s$p" "${IMAGE}/usr/$host/lib/tmpfiles.d/"*.conf; then
            ewarn "file or directory will be copied: $p"
            echo "C $p" >>"${tmpfiles_conf}"
        fi
    done
fi

# directories are all copied, despite the depth
#dirs=( $(find etc var -mindepth 1 -type d 2>/dev/null) )
#if [[ ${#dirs[@]} -gt 0 ]]; then
#    ewarn "directories found will be copied: ${dirs[@]}"
#    find ${dirs[@]} -maxdepth 0 \
#        -printf "C /%p - - - -\n" \
#        >>"${tmpfiles_conf}"
#fi

edo popd # "${IMAGE}/usr/share/factory/"

find . -name '*.la' -delete
find . -name '*.la-*' -delete
find . -mindepth 1 -type d -empty -delete
#find usr/share/info/dir -type f -delete

# systemd symlink must exist like this                                                                                                                                                           
sd_env_compat="${IMAGE}"/usr/$host/lib/environment.d/99-environment.conf                                                                                                                         
[ -L "${sd_env_compat}" ] && ln -sf ../../../../etc/environment "${sd_env_compat}"                                                                                                                

edo popd
