
# warn if absolute symlinks are created

if [[ -d "${IMAGE}" ]]; then

	for l in $(find "${IMAGE}" -type l -lpath '/*' -printf '/%P -> %l' 2>/dev/null); do
	    ewarn "absolute symlink found: ${l}"
	done

fi

