# vim: set sw=4 sts=4 et :

[[ -d "${IMAGE}" ]] || exit 0

edo pushd "${IMAGE}"

find {,usr/share/alternatives/*/*/}usr/share/{locale,man} -mindepth 1 -maxdepth 1 -type d -regextype posix-egrep ! -regex '.*/man/(man.*|pt_BR)' 2>/dev/null | xargs rm -rf

# clean empty dirs
find . -mindepth 1 -type d -empty -delete

edo popd
