#!/usr/bin/env bash

infodirfile="${IMAGE}/usr/share/info/dir"

# No package should be building nor installing the directory cache. That
# is left for the texinfo package that keeps $infodirfile as a symlink
# to ${ROOT}/var/cache/info/dir, which is the actual info dir cache.

[ -f "$infodirfile" ] && rm "$infodirfile"

true
