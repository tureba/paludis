
# if IMAGE contains a skeleton of the base layout, then
# both installs and merges get simpler and less error prone

if [[ -d "${IMAGE}" ]]; then

	local host=$(exhost --target)
#	mkdir -p "${IMAGE}"/usr/${host}

	# packages shouldn't install to /etc or /var
	# so this routes the content to a better location
	mkdir -p "${IMAGE}"/usr/share/factory/{etc,var}
	ln -s usr/share/factory/etc "${IMAGE}"/etc
	ln -s usr/share/factory/var "${IMAGE}"/var

	mkdir -p "${IMAGE}"/usr/share/doc/${PN}/${SLOT}
	ln -s ${PN}/${SLOT} "${IMAGE}"/usr/share/doc/${PNV}
	[ ${PNV} = ${PNVR} ] || ln -s ${PN}/${SLOT} "${IMAGE}"/usr/share/doc/${PNVR}

fi
