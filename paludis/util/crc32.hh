/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#ifndef PALUDIS_GUARD_PALUDIS_UTIL_CRC32_HH
#define PALUDIS_GUARD_PALUDIS_UTIL_CRC32_HH 1

#include <paludis/util/attributes.hh>

#include <fstream>

namespace paludis
{
    class CRC32
    {
        public:
            static std::string digest(std::ifstream & stream) PALUDIS_VISIBLE;
    };
}

#endif
