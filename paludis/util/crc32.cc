/* vim: set sw=4 sts=4 et foldmethod=syntax : */

#include <zlib.h>

#include <sstream>

#include <paludis/util/crc32.hh>

namespace paludis
{
    std::string CRC32::digest(std::ifstream & stream)
    {
        // save current position
        std::streampos where_was = stream.tellg();

        // get length of file:
        stream.seekg (0, stream.end);
        std::streamsize length = stream.tellg();
        stream.seekg (0, stream.beg);

        // read the whole file
        char * buffer = new char [length];
        stream.read(buffer, length);

        // initial crc32
        uLong crc = ::crc32(0L, Z_NULL, 0);

        // update with the file
        crc = ::crc32(crc, reinterpret_cast<const Bytef*>(buffer), length);

        // cleanup
        delete [] buffer;

        // restore stream position
        stream.seekg(where_was);

        // to std::string
        std::stringstream sstream;
        sstream << std::hex << crc;
        std::string result = sstream.str();

        return result;
    }
}
