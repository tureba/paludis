#!/usr/bin/env bash
# vim: set ft=sh sw=4 sts=4 et :

mkdir e_repository_sets_TEST_dir || exit 1
cd e_repository_sets_TEST_dir || exit 1

mkdir -p repo1/{eclass,distfiles,profiles/profile,sets} || exit 1
cd repo1 || exit 1

echo "test-repo-1" > profiles/repo_name || exit 1
cat <<END > profiles/categories || exit 1
cat-one
cat-two
cat-three
cat-four
END
cat <<END > profiles/profile/make.defaults
ARCH=test
END

cat <<END > sets/set1.conf
* cat-one/foo
? >=cat-two/bar-2
? <cat-three/baz-1.2
END

mkdir -p cat-one/foo cat-two/bar cat-three/baz cat-four/xyzzy
cat <<END > cat-one/foo/foo-1.ebuild
SLOT="0"
PLATFORMS="test"
END
cp cat-one/foo/foo-1.ebuild cat-one/foo/foo-2.ebuild
cp cat-one/foo/foo-1.ebuild cat-one/foo/foo-2.1.ebuild
cp cat-one/foo/foo-1.ebuild cat-two/bar/bar-1.5.ebuild
cp cat-one/foo/foo-1.ebuild cat-two/bar/bar-1.5.1.ebuild
cp cat-one/foo/foo-1.ebuild cat-two/bar/bar-2.0.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.0.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.1.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.1-r1.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.1-r2.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.2.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.2-r1.ebuild
cp cat-one/foo/foo-1.ebuild cat-three/baz/baz-1.3.ebuild
cat <<END > cat-three/baz/baz-1.3.1.ebuild
SLOT="0"
PLATFORMS="~test"
END
cat <<END > cat-four/xyzzy/xyzzy-1.1.0.ebuild
SLOT="\${PV:0:1}"
PLATFORMS="test"
END
cp cat-four/xyzzy/xyzzy-1.1.0.ebuild cat-four/xyzzy/xyzzy-1.1.1.ebuild
cp cat-four/xyzzy/xyzzy-1.1.0.ebuild cat-four/xyzzy/xyzzy-2.0.1.ebuild
cp cat-four/xyzzy/xyzzy-1.1.0.ebuild cat-four/xyzzy/xyzzy-2.0.1-r1.ebuild
cp cat-four/xyzzy/xyzzy-1.1.0.ebuild cat-four/xyzzy/xyzzy-2.0.2.ebuild
cp cat-four/xyzzy/xyzzy-1.1.0.ebuild cat-four/xyzzy/xyzzy-2.0.3.ebuild

cd ..

