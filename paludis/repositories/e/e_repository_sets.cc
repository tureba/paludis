/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2005, 2006, 2007, 2008, 2009, 2010, 2011 Ciaran McCreesh
 * Copyright (c) 2006 Danny van Dyk
 *
 * This file is part of the Paludis package manager. Paludis is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * Paludis is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <paludis/repositories/e/e_repository.hh>
#include <paludis/repositories/e/e_repository_sets.hh>
#include <paludis/repositories/e/dep_parser.hh>
#include <paludis/repositories/e/eapi.hh>

#include <paludis/action-fwd.hh>
#include <paludis/dep_spec.hh>
#include <paludis/elike_slot_requirement.hh>
#include <paludis/environment.hh>
#include <paludis/filter.hh>
#include <paludis/filtered_generator.hh>
#include <paludis/generator.hh>
#include <paludis/match_package.hh>
#include <paludis/metadata_key.hh>
#include <paludis/package_id.hh>
#include <paludis/partially_made_package_dep_spec.hh>
#include <paludis/selection.hh>
#include <paludis/set_file.hh>
#include <paludis/slot.hh>
#include <paludis/user_dep_spec.hh>
#include <paludis/version_operator.hh>
#include <paludis/version_requirements.hh>

#include <paludis/util/config_file.hh>
#include <paludis/util/is_file_with_extension.hh>
#include <paludis/util/log.hh>
#include <paludis/util/make_named_values.hh>
#include <paludis/util/pimp-impl.hh>
#include <paludis/util/sequence.hh>
#include <paludis/util/set.hh>
#include <paludis/util/strip.hh>
#include <paludis/util/tokeniser.hh>
#include <paludis/util/wrapped_forward_iterator.hh>
#include <paludis/util/fs_stat.hh>
#include <paludis/util/fs_iterator.hh>

#include <functional>
#include <algorithm>
#include <list>
#include <map>
#include <set>

#include "config.h"

using namespace paludis;
using namespace paludis::erepository;

namespace paludis
{
    /**
     * Imp data for ERepositorySets.
     *
     * \ingroup grperepository
     */
    template<>
    struct Imp<ERepositorySets>
    {
        const Environment * const environment;
        const ERepository * const e_repository;
        const erepository::ERepositoryParams params;

        Imp(const Environment * const e, const ERepository * const p,
                const erepository::ERepositoryParams & k) :
            environment(e),
            e_repository(p),
            params(k)
        {
        }
    };
}

ERepositorySets::ERepositorySets(const Environment * const e, const ERepository * const p,
        const erepository::ERepositoryParams & k) :
    _imp(e, p, k)
{
}

ERepositorySets::~ERepositorySets() = default;

const std::shared_ptr<const SetSpecTree>
ERepositorySets::package_set(const SetName & ss) const
{
    using namespace std::placeholders;

    if ("system" == ss.value())
        throw InternalError(PALUDIS_HERE, "system set should've been handled by ERepository");

    std::pair<SetName, SetFileSetOperatorMode> s(find_base_set_name_and_suffix_mode(ss));

    if ((_imp->params.setsdir() / (stringify(s.first) + ".conf")).stat().exists())
    {
        FSPath ff(_imp->params.setsdir() / (stringify(s.first) + ".conf"));
        Context context("When loading package set '" + stringify(s.first) + "' from '" + stringify(ff) + "':");

        SetFile f(make_named_values<SetFileParams>(
                    n::environment() = _imp->environment,
                    n::file_name() = ff,
                    n::parser() = std::bind(&parse_user_package_dep_spec, _1, _imp->environment, UserPackageDepSpecOptions() + updso_no_disambiguation + updso_throw_if_set, filter::All()),
                    n::set_operator_mode() = s.second,
                    n::type() = sft_paludis_conf
                    ));

        return f.contents();
    }
    else
        return nullptr;
}

std::shared_ptr<const SetNameSet>
ERepositorySets::sets_list() const
{
    Context context("While generating the list of sets:");

    std::shared_ptr<SetNameSet> result(std::make_shared<SetNameSet>());
    result->insert(SetName("system"));

    if (_imp->params.setsdir().stat().exists())
    {
        using namespace std::placeholders;

        for (FSIterator f(_imp->params.setsdir(), { }), f_end ; f != f_end ; ++f)
        {
            if (! is_file_with_extension(*f, ".conf", { }))
                continue;

            try
            {
                result->insert(SetName(strip_trailing_string(f->basename(), ".conf")));
            }
            catch (const NameError & e)
            {
                Log::get_instance()->message("e.sets.failure", ll_warning, lc_context) << "Skipping set '"
                    << *f << "' due to exception '" << e.message() << "' (" << e.what() << ")";
            }
        }
    }

    return result;
}

